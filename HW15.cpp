#include <iostream>

static int P = 10;

void FindEvenNumbers()

//print even numbers V1(const)
{
    std::cout << "even numbers V1(const)\n";
    for (int i = 0; i <= P; i++)
    {
        if (i % 2) {}
        else { std::cout << i << "\n"; }
    }
}

void FindOddNumbers()
{
    //print odd numbers V1(const)
    std::cout << "odd numbers V1(const)\n";
    for (int i = 0; i <= P; i++)
    {
        if (i % 2) { std::cout << i << "\n"; }
        else {}
    }
}

void FindEvenNumberV2()

{
    std::cout << "Enter Limit: ";
    int Limit;
    std::cin >> Limit;
    //print even numbers V2(user parametr)
    std::cout << "even numbers V2(user parametr)\n";
    
    for (int j = 0; j <= Limit; j++)
    {
        if (j % 2 == 0) { std::cout << j << "\n"; }
    }  
}

void FindOddNumbersV2()
{
    std::cout << "Enter Limit: ";
    int Limit;
    std::cin >> Limit;
    //print even numbers V2(user parametr)
    std::cout << "even numbers V2(user parametr)\n";

    for (int j = 0; j <= Limit; j++)
    {
        if (j % 2 == 1) { std::cout << j << "\n"; }
    }
}

void FindEvenNumberV3()
{
    std::cout << "Enter Limit: ";
    int Limit;
    std::cin >> Limit;
    //print even numbers V2(user parametr)
    std::cout << "even numbers V2(user parametr)\n";

    for (int j = 0; j <= Limit; j += 2) // j = j + 2 (0 2 4 .. N)
    {
        std::cout << j << "\n";;
    }

}

void FindOddNumbersV3()
{
    std::cout << "Enter Limit: ";
    int Limit;
    std::cin >> Limit;
    //print even numbers V2(user parametr)
    std::cout << "even numbers V2(user parametr)\n";

    //print odd numbers V3(no if)
    std::cout << "odd numbers V3(no if)\n";
    for (int j = 1; j <= Limit; j += 2) // 1 3 5 ...N
    {
        std::cout << j << "\n";;
    }

}


int main()
{
    FindEvenNumbers();
    FindOddNumbers();
    std::cout << "\n";
    FindEvenNumberV2();
    FindOddNumbersV2();
    std::cout << "\n";
    FindEvenNumberV3();
    FindOddNumbersV3();
       
}

